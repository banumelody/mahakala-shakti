//
//  OutputViewController.m
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 12/20/14.
//  Copyright (c) 2014 Banu Antoro. All rights reserved.
//

#import "OutputViewController.h"
#import "ListTableViewCell.h"

@interface OutputViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *OMLabel;
@property (weak, nonatomic) IBOutlet UILabel *karmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *skLabel;
@property (weak, nonatomic) IBOutlet UILabel *ddharmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *DharmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *idaLabel;
@property (weak, nonatomic) IBOutlet UILabel *caLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation OutputViewController {
    TanggalLahir *tL;
    Karakter *kar;
    NSMutableArray *nListArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    _OMLabel.text = [NSString stringWithFormat:@"OM =%@", [self convertNameNumToOM:_nameNum]];
    
    // Tanggal Lahir & karma
    tL = [self convertTanggalLahirToKarma:_tanggalLahirString];
    
    _idaLabel.text = [NSString stringWithFormat:@"ida:%d shu:%ld ping:%d", tL.bl, (long)tL.tl, tL.thl];
    
    _skLabel.text = [NSString stringWithFormat:@"sk:%ld pk:%d ik:%d mk:%d", (long)tL.sk, tL.pk, tL.ik, tL.mk];
    _karmaLabel.text = [NSString stringWithFormat:@"karma = %d", tL.karma];
    
    int dharma = [self modulo9:(abs(tL.karma-self.OM))];
    _ddharmaLabel.text = [NSString stringWithFormat:@"dharma = %d", dharma];
    
    int Dharma = [self modulo9exept11_13_16:(tL.karma+self.OM)];
    _DharmaLabel.text = [NSString stringWithFormat:@"Dharma = %d", Dharma];
    
    self.title = [NSString stringWithFormat:@"%@ | %@", _name, _tanggalLahirString];
    
    kar = [[Karakter alloc] initWithNumOfName:_nameNum];
    
    _caLabel.text = [NSString stringWithFormat:@"Karakter = %d | %d | %d | %d", kar.fisik, kar.mental, kar.emosi, kar.intuisi];
    
    nListArray = [[NSMutableArray alloc] init];
    
    for (NSString *word in _nameNum) {
        [nListArray addObject:[self generateNList:word]];
    }
    
//    for (int i = 0; i<[_nameNum count]; i++) {
////        [nListArray addObject:[self generateNList:_nameNum[i]]];
//        nListArray[i] = [self generateNList:_nameNum[i]];
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}

// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    cell.textLabel.text = [NSString stringWithFormat:@"%d", indexPath.row];
    
    ListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    
    cell.umurLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    cell.tahunLabel.text = [NSString stringWithFormat:@"%d", tL.thn + indexPath.row];
    
    NSString *tahun = [NSString stringWithFormat:@"%d", tL.thn+indexPath.row];
    int thn = 0;
    
    for (int i = 0; i<tahun.length; i++) {
        NSRange range = NSMakeRange(i, 1);
        thn = thn + [[tahun substringWithRange:range] integerValue];
    }
    
    cell.latLabel.text = [NSString stringWithFormat:@"%d", [self modulo9exept11_22:thn]];
    int sm = [self modulo9exept11_13_14:([cell.latLabel.text integerValue] + tL.sk)];
    cell.smLabel.text = [NSString stringWithFormat:@"%d", sm];

    
    for (int i = 0; i<[cell.nameLabel count]; i++) {
        ((UILabel *)cell.nameLabel[i]).text = @"-";
        if (i < [nListArray count]) {
            ((UILabel *)cell.nameLabel[i]).text = nListArray[i][indexPath.row];
        }
    }
    
    int vr = 0;
    for (NSMutableArray *numOfN in nListArray) {
        vr = vr + [numOfN[indexPath.row] integerValue];
    }
    
    if (abs(vr-sm) == 3) {
        [cell.vrLabel setTextColor:[UIColor redColor]];
        [cell.smLabel setTextColor:[UIColor redColor]];
    }
    
    cell.vrLabel.text = [NSString stringWithFormat:@"%d", [self modulo9exept11_13_14_16_19_22:vr]];
    
//    cell.n1Label.text = (nListArray[0]? nListArray[0][indexPath.row]:@"-");
//    cell.n2Label.text = (nListArray[1]? nListArray[1][indexPath.row]:@"-");
//    cell.n3Label.text = (nListArray[2]? nListArray[2][indexPath.row]:@"-");
//    cell.n4Label.text = (nListArray[3]? nListArray[3][indexPath.row]:@"-");
//    cell.n5Label.text = (nListArray[4]? nListArray[4][indexPath.row]:@"-");
//    cell.n6Label.text = (nListArray[5]? nListArray[5][indexPath.row]:@"-");
    
    
//    cell.n1Label.text = nListArray[0][indexPath.row];
//    cell.n2Label.text = nListArray[1][indexPath.row];
//    cell.n3Label.text = nListArray[2][indexPath.row];
//    cell.n4Label.text = nListArray[3][indexPath.row];
//    cell.n5Label.text = nListArray[4][indexPath.row];
//    cell.n6Label.text = nListArray[5][indexPath.row];
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 50)];
    header.backgroundColor = [UIColor redColor];
    return header;
}

@end
