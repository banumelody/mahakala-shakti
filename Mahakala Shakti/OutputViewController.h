//
//  OutputViewController.h
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 12/20/14.
//  Copyright (c) 2014 Banu Antoro. All rights reserved.
//

#import "ViewController.h"

@interface OutputViewController : ViewController

@property (nonatomic, strong) NSMutableArray *nameNum;
@property (nonatomic, strong) NSString *tanggalLahirString;
@property (nonatomic, strong) NSString *name;


@end