//
//  ListTableViewCell.m
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 1/25/15.
//  Copyright (c) 2015 Banu Antoro. All rights reserved.
//

#import "ListTableViewCell.h"

@implementation ListTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _nameLabel = [[NSMutableArray alloc] initWithObjects:_n1Label, _n2Label, _n3Label, _n4Label, _n5Label, _n6Label, nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
