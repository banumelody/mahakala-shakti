//
//  FormView.h
//  Mahakala Shakti
//
//  Created by Banu Antoro on 2/16/15.
//  Copyright (c) 2015 Banu Antoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHS.h"

@interface FormView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nama;
@property (weak, nonatomic) IBOutlet UITextField *tglLahir;

@property (weak, nonatomic) IBOutlet UILabel *OMLabel;
@property (weak, nonatomic) IBOutlet UILabel *karmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *skLabel;
@property (weak, nonatomic) IBOutlet UILabel *ddharmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *DharmaLabel;
@property (weak, nonatomic) IBOutlet UILabel *idaLabel;
@property (weak, nonatomic) IBOutlet UILabel *caLabel;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *dCLabel;




@end
