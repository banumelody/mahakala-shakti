//
//  MHS.h
//  Mahakala Shakti
//
//  Created by Banu Antoro on 2/16/15.
//  Copyright (c) 2015 Banu Antoro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Karakter : NSObject

@property (nonatomic) NSMutableArray *numOfName;

@property (nonatomic) int fisik;
@property (nonatomic) int mental;
@property (nonatomic) int emosi;
@property (nonatomic) int intuisi;

- (id)initWithNumOfName:(NSMutableArray *)numOfName;

@end


@interface TanggalLahir : NSObject

@property (nonatomic) NSInteger tl;
@property (nonatomic) int bl;
@property (nonatomic) int thl;
@property (nonatomic) int karma;
@property (nonatomic) NSInteger sk;
@property (nonatomic) int pk;
@property (nonatomic) int ik;
@property (nonatomic) int mk;

@property (nonatomic) NSInteger tgl;
@property (nonatomic) NSInteger bln;
@property (nonatomic) NSInteger thn;

- (id)initWithTanggalLahirString:(NSString *)tanggalLahirString;
- (int)modulo9exept11_22:(int)value;

@end

@interface MHS : NSObject

@property (nonatomic, strong) NSString *oMText;
@property (nonatomic, strong) NSString *caText;
@property (nonatomic, strong) NSString *idaText;
@property (nonatomic, strong) NSString *skText;
@property (nonatomic, strong) NSString *karmaText;
@property (nonatomic, strong) NSString *DharmaText;
@property (nonatomic, strong) NSString *ddharmaText;
@property (nonatomic, strong) NSMutableArray *nameNum ;
@property (nonatomic, strong) NSString *dCParrent;
@property (nonatomic, strong) NSMutableArray *dCLine;
@property (nonatomic) TanggalLahir *tL;
@property (nonatomic, strong) NSString *deathCodeText;
@property (nonatomic, strong) NSString *namaLengkap;


@property (nonatomic) Karakter *karakter;
@property (nonatomic) int OM;
@property (strong, nonatomic) NSMutableDictionary *PYTHAGORASNUMEROLOGY;

- (NSMutableArray *)convertNameToNum:(NSString *)name;
- (NSString *)convertNameNumToOM:(NSArray *)nameNum;
- (TanggalLahir *)convertTanggalLahirToKarma:(NSString *)tanggalLahirString;
- (int)modulo9exept11_13_16:(int)value;
- (int)modulo9:(int)value;
- (int)modulo9exept11_22:(int)value;
- (int)modulo9exept11_13_14:(int)value;
- (NSMutableArray *)generateNList:(NSString *)name;
- (int)modulo9exept11_13_14_16_19_22:(int)value;

- (id)initWithName:(NSString *)namalengkap withBirthDate:(NSString *)birthDate;

@end
