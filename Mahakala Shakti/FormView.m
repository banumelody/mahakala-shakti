//
//  FormView.m
//  Mahakala Shakti
//
//  Created by Banu Antoro on 2/16/15.
//  Copyright (c) 2015 Banu Antoro. All rights reserved.
//

#import "FormView.h"
#import "ListTableViewCell.h"

@implementation FormView {
    NSMutableArray *nListArray;
    MHS *mahakalaShakti;
    NSMutableArray *m1;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    
    [_tglLahir setInputView:datePicker];
    
}

- (void) dateTextField:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)_tglLahir.inputView;
    //    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _tglLahir.text = [NSString stringWithFormat:@"%@",dateString];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (IBAction)submit:(id)sender {
    mahakalaShakti = [[MHS alloc] initWithName:_nama.text withBirthDate:_tglLahir.text];
    
    // Do any additional setup after loading the view.
    
    _OMLabel.text = mahakalaShakti.oMText;

    // Tanggal Lahir & karma
    _idaLabel.text = mahakalaShakti.idaText;
    
    _skLabel.text = mahakalaShakti.skText;
    _karmaLabel.text = mahakalaShakti.karmaText;
    
    _ddharmaLabel.text = mahakalaShakti.ddharmaText;

    _DharmaLabel.text = mahakalaShakti.DharmaText;
    
    _caLabel.text = mahakalaShakti.caText;
    
    _dCLabel.text = mahakalaShakti.deathCodeText;
    
    nListArray = [[NSMutableArray alloc] init];
    
    if (_nama.text.length > 0) {
        for (NSString *word in mahakalaShakti.nameNum) {
            [nListArray addObject:[mahakalaShakti generateNList:word]];
        }
    }


    _tableView.delegate = self;
    _tableView.dataSource = self;
    [_tableView reloadData];
    [self endEditing:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *topLevelObj = [[NSBundle mainBundle] loadNibNamed:@"FormView" owner:self options:nil];

    //etc.
    ListTableViewCell *cell = [topLevelObj objectAtIndex:1];
    
    cell.umurLabel.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    cell.tahunLabel.text = [NSString stringWithFormat:@"%d", mahakalaShakti.tL.thn + indexPath.row];
    
    NSString *tahun = [NSString stringWithFormat:@"%d", mahakalaShakti.tL.thn+indexPath.row];
    NSInteger thn = 0;
    
    for (int i = 0; i<tahun.length; i++) {
        NSRange range = NSMakeRange(i, 1);
        thn = thn + [[tahun substringWithRange:range] integerValue];
    }
    
    cell.latLabel.text = [NSString stringWithFormat:@"%d", [mahakalaShakti modulo9exept11_22:(int)thn]];
    NSInteger sm = [mahakalaShakti modulo9exept11_13_14:([cell.latLabel.text integerValue] + mahakalaShakti.tL.sk)];
    cell.smLabel.text = [NSString stringWithFormat:@"%ld", (long)sm];
    
    
    for (int i = 0; i<[cell.nameLabel count]; i++) {
        ((UILabel *)cell.nameLabel[i]).text = @"-";
        if (i < [nListArray count]) {
            if ([nListArray[i] count] > 0) {
                ((UILabel *)cell.nameLabel[i]).text = nListArray[i][indexPath.row];
            }
        }
    }
    
    NSInteger vr = 0;
    for (NSMutableArray *numOfN in nListArray) {
        vr = vr + [numOfN[indexPath.row] integerValue];
    }
    
    cell.vrLabel.text = [NSString stringWithFormat:@"%d", [mahakalaShakti modulo9exept11_13_14_16_19_22:vr]];
    
    if (abs([cell.vrLabel.text integerValue]-[cell.smLabel.text integerValue]) == 3) {
        [cell.vrLabel setTextColor:[UIColor redColor]];
        [cell.smLabel setTextColor:[UIColor redColor]];
    }
    
    NSInteger m1Index = (indexPath.row/9)%8;
    cell.m1Label.text = [NSString stringWithFormat:@"%@", mahakalaShakti.dCLine[1][m1Index]];
    switch ([cell.m1Label.text integerValue]) {
        case 8:
        case 9:
        case 13:
            [cell.m1Label setTextColor:[UIColor redColor]];
            break;
            
        case 5:
        case 1:
        case 14:
            [cell.m1Label setTextColor:[UIColor orangeColor]];
            break;
            
        default:
            [cell.m1Label setTextColor:[UIColor blackColor]];
            break;
    }
    
    NSInteger m2Index = (indexPath.row/9)%9;
    cell.m2Label.text = [NSString stringWithFormat:@"%@", mahakalaShakti.dCLine[0][m2Index]];
    switch ([cell.m2Label.text integerValue]) {
        case 8:
        case 9:
        case 13:
            [cell.m2Label setTextColor:[UIColor redColor]];
            break;
            
        case 5:
        case 1:
        case 14:
            [cell.m2Label setTextColor:[UIColor orangeColor]];
            break;
            
        default:
            [cell.m2Label setTextColor:[UIColor blackColor]];
            break;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 100;
}

@end
