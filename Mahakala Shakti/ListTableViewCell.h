//
//  ListTableViewCell.h
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 1/25/15.
//  Copyright (c) 2015 Banu Antoro. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewCell : UITableViewCell

@property (nonatomic) NSMutableArray *nameLabel;

@property (weak, nonatomic) IBOutlet UILabel *umurLabel;
@property (weak, nonatomic) IBOutlet UILabel *tahunLabel;
@property (weak, nonatomic) IBOutlet UILabel *latLabel;
@property (weak, nonatomic) IBOutlet UILabel *smLabel;
@property (weak, nonatomic) IBOutlet UILabel *n1Label;
@property (weak, nonatomic) IBOutlet UILabel *n2Label;
@property (weak, nonatomic) IBOutlet UILabel *n3Label;
@property (weak, nonatomic) IBOutlet UILabel *n4Label;
@property (weak, nonatomic) IBOutlet UILabel *n5Label;
@property (weak, nonatomic) IBOutlet UILabel *n6Label;
@property (weak, nonatomic) IBOutlet UILabel *vrLabel;
@property (weak, nonatomic) IBOutlet UILabel *m1Label;
@property (weak, nonatomic) IBOutlet UILabel *m2Label;

@end
