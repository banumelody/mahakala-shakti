//
//  ViewController.h
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 12/20/14.
//  Copyright (c) 2014 Banu Antoro. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHS.h"

//@interface Karakter : NSObject
//
//@property (nonatomic) NSMutableArray *numOfName;
//
//@property (nonatomic) int fisik;
//@property (nonatomic) int mental;
//@property (nonatomic) int emosi;
//@property (nonatomic) int intuisi;
//
//- (id)initWithNumOfName:(NSMutableArray *)numOfName;
//
//@end
//
//
//@interface TanggalLahir : NSObject
//
//@property (nonatomic) int tl;
//@property (nonatomic) int bl;
//@property (nonatomic) int thl;
//@property (nonatomic) int karma;
//@property (nonatomic) int sk;
//@property (nonatomic) int pk;
//@property (nonatomic) int ik;
//@property (nonatomic) int mk;
//
//@property (nonatomic) int tgl;
//@property (nonatomic) int bln;
//@property (nonatomic) int thn;
//
//- (id)initWithTanggalLahirString:(NSString *)tanggalLahirString;
//- (int)modulo9exept11_22:(int)value;
//
//@end

@interface MahakalaShakti : NSObject

@property (nonatomic) Karakter *karakter;
@property (nonatomic) int OM;
@property (strong, nonatomic) NSMutableDictionary *PYTHAGORASNUMEROLOGY;

- (NSMutableArray *)convertNameToNum:(NSString *)name;
- (NSString *)convertNameNumToOM:(NSArray *)nameNum;
- (TanggalLahir *)convertTanggalLahirToKarma:(NSString *)tanggalLahirString;
- (int)modulo9exept11_13_16:(int)value;
- (int)modulo9:(int)value;
- (int)modulo9exept11_22:(int)value;
- (int)modulo9exept11_13_14:(int)value;
- (NSMutableArray *)generateNList:(NSString *)name;
- (int)modulo9exept11_13_14_16_19_22:(int)value;


@end

@interface ViewController : UIViewController

@property (nonatomic) Karakter *karakter;
@property (nonatomic) int OM;

- (NSMutableArray *)convertNameToNum:(NSString *)name;
- (NSString *)convertNameNumToOM:(NSArray *)nameNum;
- (TanggalLahir *)convertTanggalLahirToKarma:(NSString *)tanggalLahirString;
- (int)modulo9exept11_13_16:(int)value;
- (int)modulo9:(int)value;
- (int)modulo9exept11_22:(int)value;
- (int)modulo9exept11_13_14:(int)value;
- (NSMutableArray *)generateNList:(NSString *)name;
- (int)modulo9exept11_13_14_16_19_22:(int)value;
@end



