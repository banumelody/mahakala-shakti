//
//  ViewController.m
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 12/20/14.
//  Copyright (c) 2014 Banu Antoro. All rights reserved.
//

#import "ViewController.h"
#import <Crashlytics/Crashlytics.h>


@interface ViewController ()

@property (strong, nonatomic) NSMutableDictionary *PYTHAGORASNUMEROLOGY;

@end

@implementation ViewController {

}

- (void)loadView {
    [super loadView];
    if (!_PYTHAGORASNUMEROLOGY) {
        [self initPythNum];
    }
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(20, 50, 100, 30);
    [button setTitle:@"Crash" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(crashButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];

}

- (IBAction)crashButtonTapped:(id)sender {
    [[Crashlytics sharedInstance] crash];
}


- (void)initPythNum {
    _PYTHAGORASNUMEROLOGY = [[NSMutableDictionary alloc] init];
    
    NSString *string = @"a";
    int asciiCode = [string characterAtIndex:0];
    
    for (int i=1; i <= 26; i++) {
        //        NSLog(@"ascii = %d", asciiCode + i - 1);
        NSString *huruf = [NSString stringWithFormat:@"%c", asciiCode + i - 1];
        
        int angk = i%9;
        if (angk == 0) {
            angk = 9;
        }
        
        NSString *angka = [NSString stringWithFormat:@"%d", angk];
        
        [_PYTHAGORASNUMEROLOGY setObject:angka forKey:huruf];
        
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSMutableArray *)convertNameToNum:(NSString *)name {
    name = [name lowercaseString];
    NSMutableArray *names = [[NSMutableArray alloc] init];
    
    NSArray *myWords = [name componentsSeparatedByString:@" "];
    
    for (int i = 0; i < [myWords count]; i++) {
        NSString *wordNum = @"";
        for (int j = 0; j < ((NSString *) myWords[i]).length; j++) {
            NSString *huruf = [((NSString *) myWords[i]) substringWithRange:NSMakeRange(j, 1)];
            wordNum = [NSString stringWithFormat:@"%@%@", wordNum, _PYTHAGORASNUMEROLOGY[huruf]];
            
            
        }
        
        [names addObject:wordNum];
    }
    
    _karakter = [[Karakter alloc] initWithNumOfName:names];
    
    return names;
}

- (NSString *)convertNameNumToOM:(NSArray *)nameNum {
    NSString *jumlahNama = @"";
    
    int jumlahOM = 0;
    
    for (NSString *name in nameNum) {
        int jumlah = 0;
        for (int i = 0; i < name.length; i++) {
            NSRange range = NSMakeRange(i, 1);
            jumlah = jumlah + [[name substringWithRange:range] intValue];
        }
        
        jumlahOM = jumlahOM + jumlah;
        
        jumlahNama = [NSString stringWithFormat:@"%@ | %d", jumlahNama, jumlah];
    }
    
    int OM = [self modulo9exept11_22:jumlahOM];
    
    _OM = OM;
    jumlahNama = [NSString stringWithFormat:@"%@ | %d : %d", jumlahNama, jumlahOM, OM];
    return jumlahNama;
}

- (TanggalLahir *)convertTanggalLahirToKarma:(NSString *)tanggalLahirString {
    TanggalLahir *tanggalLahir = [[TanggalLahir alloc] initWithTanggalLahirString:tanggalLahirString];
    
    return tanggalLahir;
}

- (int)modulo9exept11_13_16:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 16:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }

            break;
    }
    
    return result;
}

- (int)modulo9exept11_13_14:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 14:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }
            
            break;
    }
    
    return result;
}

- (int)modulo9exept11_13_14_16_19_22:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 14:
            return result;
            break;
        case 16:
            return result;
            break;
        case 19:
            return result;
            break;
        case 22:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }
            
            break;
    }
    
    return result;
}


- (int)modulo9:(int)value {
    int result = value%9;
    if (result==0) {
        result = 9;
    }
    return result;
}

- (int)modulo9exept11_22:(int)value {
    int result = value;
    if (value==11 || value==22) {
        return result;
    }
    
    result = value%9;
    if (result==0) {
        result = 9;
    }
    
    return result;
}

- (NSMutableArray *)generateNList:(NSString *)name {
    NSMutableArray *numOfFont = [[NSMutableArray alloc] init];
    for (int i = 0; i<name.length; i++) {
        NSRange range = NSMakeRange(i, 1);
        [numOfFont addObject:[name substringWithRange:range]];
    }
    
    NSMutableArray *nList = [[NSMutableArray alloc] init];
    
    int index = 0;
    int numIndex = 0;
    do {
        for (int i = 0; i<[numOfFont[numIndex] integerValue]; i++) {
            [nList addObject:numOfFont[numIndex]];
            index++;
        }
        numIndex++;
        if (numIndex == name.length) {
            numIndex = 0;
        }
        
    } while (index<100);
    
    return nList;
}

@end

//@implementation TanggalLahir {
//    
//}
//
//- (id)initWithTanggalLahirString:(NSString *)tanggalLahirString {
//    self = [super init];
//    if (self && tanggalLahirString.length>0) {
//        NSArray *date = [tanggalLahirString componentsSeparatedByString:@"/"];
//        _tgl = [date[0] integerValue];
//        _bln = [date[1] integerValue];
//        _thn = [date[2] integerValue];
//        
//        
//        _tl = [self modulo9exept11_22:_tgl];
//        _bl = [self modulo9exept11_22:_bln];
//        _thl = [self modulo9exept11_22:_thn];
//        _karma = [self modulo9exept11_22:(_tl+_bl+_thl)];
//        
//        _sk = [self modulo9exept11_22:(_tl+_bl)];
//        _pk = [self modulo9exept11_22:(_tl+_thl)];
//        _ik = [self modulo9exept11_22:(_sk+_pk)];
//        _mk = [self modulo9exept11_22:(_thl+_bl)];
//        
//    }
//    
//    return self;
//}
//
//- (int)modulo9exept11_22:(int)value {
//    int result = value;
//    if (value==11 || value==22) {
//        return result;
//    }
//    
//    result = value%9;
//    if (result==0) {
//        result = 9;
//    }
//    
//    return result;
//}
//
//
//@end
//
//
//@implementation Karakter {
//    
//}
//
//- (id)initWithNumOfName:(NSMutableArray *)numOfName {
//    self = [super init];
//    
//    if (self) {
//        _numOfName = numOfName;
//        NSLog(@"NumOfName = %@", numOfName);
//        [self convert];
//        NSLog(@"FISIK = %d", _fisik);
//    }
//    
//    return self;
//}
//
//- (void)convert {
//    _fisik = 0;
//    _mental = 0;
//    _emosi = 0;
//    _intuisi = 0;
//    for (NSString *nums in _numOfName) {
//        
//        for (int i = 0; i < nums.length; i++) {
//            NSRange range = NSMakeRange(i, 1);
////            jumlah = jumlah + [[num substringWithRange:range] intValue];
//            
//            int num = [[nums substringWithRange:range] integerValue];
//            
//            if ((num == 4) || (num == 5)) {
//                
//            }
//            
//            switch (num) {
//                case 4 :
//                    _fisik = _fisik + 1;
//                    break;
//                    
//                case 5:
//                    _fisik = _fisik + 1;
//                    break;
//                
//                case 1:
//                    _mental = _mental + 1;
//                    break;
//                    
//                case 8:
//                    _mental = _mental + 1;
//                    break;
//                    
//                case 2:
//                    _emosi = _emosi + 1;
//                    break;
//
//                case 3:
//                    _emosi = _emosi + 1;
//                    break;
//                
//                case 6:
//                    _emosi = _emosi + 1;
//                    break;
//                    
//                case 7:
//                    _intuisi = _intuisi + 1;
//                    break;
//                    
//                case 9:
//                    _intuisi = _intuisi + 1;
//                    break;
//                    
//                default:
//                    break;
//            }
//            
//        }
//        
////        jumlahOM = jumlahOM + jumlah;
////        
////        jumlahNama = [NSString stringWithFormat:@"%@ | %d", jumlahNama, jumlah];
//    }
//    
//}
//
//
//@end

@implementation MahakalaShakti


- (void)initPythNum {
    _PYTHAGORASNUMEROLOGY = [[NSMutableDictionary alloc] init];
    
    NSString *string = @"a";
    int asciiCode = [string characterAtIndex:0];
    
    for (int i=1; i <= 26; i++) {
        //        NSLog(@"ascii = %d", asciiCode + i - 1);
        NSString *huruf = [NSString stringWithFormat:@"%c", asciiCode + i - 1];
        
        int angk = i%9;
        if (angk == 0) {
            angk = 9;
        }
        
        NSString *angka = [NSString stringWithFormat:@"%d", angk];
        
        [_PYTHAGORASNUMEROLOGY setObject:angka forKey:huruf];
        
    }
    
}


- (NSMutableArray *)convertNameToNum:(NSString *)name {
    name = [name lowercaseString];
    NSMutableArray *names = [[NSMutableArray alloc] init];
    
    NSArray *myWords = [name componentsSeparatedByString:@" "];
    
    for (int i = 0; i < [myWords count]; i++) {
        NSString *wordNum = @"";
        for (int j = 0; j < ((NSString *) myWords[i]).length; j++) {
            NSString *huruf = [((NSString *) myWords[i]) substringWithRange:NSMakeRange(j, 1)];
            wordNum = [NSString stringWithFormat:@"%@%@", wordNum, _PYTHAGORASNUMEROLOGY[huruf]];
            
            
        }
        
        [names addObject:wordNum];
    }
    
    _karakter = [[Karakter alloc] initWithNumOfName:names];
    
    return names;
}

- (NSString *)convertNameNumToOM:(NSArray *)nameNum {
    NSString *jumlahNama = @"";
    
    int jumlahOM = 0;
    
    for (NSString *name in nameNum) {
        int jumlah = 0;
        for (int i = 0; i < name.length; i++) {
            NSRange range = NSMakeRange(i, 1);
            jumlah = jumlah + [[name substringWithRange:range] intValue];
        }
        
        jumlahOM = jumlahOM + jumlah;
        
        jumlahNama = [NSString stringWithFormat:@"%@ | %d", jumlahNama, jumlah];
    }
    
    int OM = [self modulo9exept11_22:jumlahOM];
    
    _OM = OM;
    jumlahNama = [NSString stringWithFormat:@"%@ | %d : %d", jumlahNama, jumlahOM, OM];
    return jumlahNama;
}

- (TanggalLahir *)convertTanggalLahirToKarma:(NSString *)tanggalLahirString {
    TanggalLahir *tanggalLahir = [[TanggalLahir alloc] initWithTanggalLahirString:tanggalLahirString];
    
    
    
    return tanggalLahir;
}

- (int)modulo9exept11_13_16:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 16:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }
            
            break;
    }
    
    return result;
}

- (int)modulo9exept11_13_14:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 14:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }
            
            break;
    }
    
    return result;
}


- (int)modulo9exept11_13_14_16_19_22:(int)value {
    int result = value;
    
    switch (result) {
        case 11:
            return result;
            break;
        case 13:
            return result;
            break;
        case 14:
            return result;
            break;
        case 16:
            return result;
            break;
        case 19:
            return result;
            break;
        case 22:
            return result;
            break;
        default:
            result = value%9;
            if (result==0) {
                result = 9;
            }
            
            break;
    }
    
    return result;
}


- (int)modulo9:(int)value {
    int result = value%9;
    if (result==0) {
        result = 9;
    }
    return result;
}

- (int)modulo9exept11_22:(int)value {
    int result = value;
    if (value==11 || value==22) {
        return result;
    }
    
    result = value%9;
    if (result==0) {
        result = 9;
    }
    
    return result;
}

- (NSMutableArray *)generateNList:(NSString *)name {
    NSMutableArray *numOfFont = [[NSMutableArray alloc] init];
    for (int i = 0; i<name.length; i++) {
        NSRange range = NSMakeRange(i, 1);
        [numOfFont addObject:[name substringWithRange:range]];
    }
    
    NSMutableArray *nList = [[NSMutableArray alloc] init];
    
    int index = 0;
    int numIndex = 0;
    do {
        for (int i = 0; i<[numOfFont[numIndex] integerValue]; i++) {
            [nList addObject:numOfFont[numIndex]];
            index++;
        }
        numIndex++;
        if (numIndex == name.length) {
            numIndex = 0;
        }
        
    } while (index<100);
    
    return nList;
}


@end

