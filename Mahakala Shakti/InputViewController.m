//
//  InputViewController.m
//  Mahakala Shakti
//
//  Created by Banu Desi Antoro on 12/20/14.
//  Copyright (c) 2014 Banu Antoro. All rights reserved.
//

#import "InputViewController.h"
#import "OutputViewController.h"

@interface InputViewController ()
@property (weak, nonatomic) IBOutlet UITextField *tanggalLahir;
@property (weak, nonatomic) IBOutlet UITextField *nama;

@end

@implementation InputViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    
    [_tanggalLahir setInputView:datePicker];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) dateTextField:(id)sender
{
    UIDatePicker *picker = (UIDatePicker*)_tanggalLahir.inputView;
//    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    _tanggalLahir.text = [NSString stringWithFormat:@"%@",dateString];
}

- (IBAction)submitAction:(id)sender {
    NSMutableArray *wordName = [self convertNameToNum:_nama.text];
    
    for (NSString *word in wordName) {
        NSLog(@"name = %@", word);
    }
   
    OutputViewController *outputViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"output"];
    outputViewController.nameNum = wordName;
    outputViewController.tanggalLahirString = _tanggalLahir.text;
    outputViewController.name = _nama.text;
    
    [self.navigationController showViewController:outputViewController sender:sender];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
